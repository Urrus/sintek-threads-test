#include "stdafx.h"
#include "GeneratorSingle.h"
#include <iostream>
#include "GenDataSingleVal.h"

using namespace std;

GeneratorSingle::GeneratorSingle(ITCBuffer* buffer, GeneratorSingleConfig* config)
	: GeneratorBase(buffer, config)
{
	
}

GeneratorSingle::~GeneratorSingle(void)
{
}

QueueDataBase* GeneratorSingle::GenerateData()
{
	auto cfg = _GetConfig();
	GenDataSingleVal* res;
	//���� � ���������� ���� �������������� ������������������ ��������, �� ������ ��������� ������� �� ���
	if(cfg->sequence != NULL)
	{
		if(_genCount == cfg->sequence->size() - 1)
		{
			_isStoppingThread = true; // ���� ������ ��������� ������� - ������������� ������ ����������
		}
		res = cfg->sequence->at(_genCount);
	}
	else
	{
		auto col = _NextColor();
		res = new GenDataSingleVal(col, _genCount);
	}

#ifndef SNT_DEBUG_MUTE
	tsout << "Element generated: id=" << res->GetId() << "; color=" << res->GetColor() << "\n";
#endif

	return res;
}

color GeneratorSingle::_NextColor()
{
	color res = rand() % 3;
	return res;
}

GeneratorSingleConfig* GeneratorSingle::_GetConfig()
{
	return (GeneratorSingleConfig*) _config;
}

GeneratorSingleConfig* GeneratorSingle::GetConfig()
{
	return _GetConfig();
}