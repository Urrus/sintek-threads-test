#pragma once
#include <map>
#include "ThreadSafeCout.h"
#include "GenDataSingleVal.h"
#include <vector>
#include "color.h"

//#define SNT_DEBUG_MUTE
#define SNT_DEBUG_THREADSLEEP

//��������� ����������
struct MainConfig
{
	//��������� ������� ��� ������.
	//��������, ��� ����������� ����� � ������������� ��������, ��������� ������� � ���������� ��������� ����� �������.
	std::map<color, int> order;

	//������ � �������� ������
	std::string orderStr;

};

//������� ��������� ����������
struct GeneratorBaseConfig
{
	//������ ��������� ������ �������� � ������������
	double genPeriodMill;

	//���������� ���������, ������� ����� �������������. ���� 0 - �� ��������� �� ��������� ������.
	long int genAmount;

	virtual ~GeneratorBaseConfig()
	{
	}
};

//��������� ��� ���������� ��������� � ������������ ��������� �����
struct  GeneratorSingleConfig : GeneratorBaseConfig
{
	//���������������� ������������������ ��� ����������. ���� NULL, �� ����� �������������� ��������� �������� �����
	std::vector<GenDataSingleVal*>* sequence;
	GeneratorSingleConfig()
		: sequence(NULL)
	{
	}

	~GeneratorSingleConfig()
	{
	}
};

//������� ��������� ������
struct  ReaderBaseConfig
{
	//������ ������ ������� �������� � ������������
	double readPeriodMill;
	
	//��������� ������� ��� ������
	std::map<color, int> order;

	//���������� ���������, ������� ����� �������������. ���� 0 - �� ������ �� ��������� ������.
	long int readAmount;

	virtual ~ReaderBaseConfig()
	{
	}
};

//��������� ��� ����������� ��������� � ������������ ��������� �����
struct ReaderSingleConfig : ReaderBaseConfig
{
	~ReaderSingleConfig()
	{
	}
};

//������ ��� ����������������� ������ � std::cout
extern ThreadSafeCout tsout;