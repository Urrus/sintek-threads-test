#pragma once
#include "generatorbase.h"
#include "ITCBuffer.h"
#include "General.h"
#include <random>
#include "QueueDataBase.h"

//����� ��� ���������� ���������, ���������� ������������ �������� �����
class GeneratorSingle :
	public GeneratorBase
{
protected:
	//��������� ������ �������� �����
	virtual color _NextColor();

	//����� ��� ��������� ������������ ��� ����������� ���������� �� ���� �������� ������ _config
	GeneratorSingleConfig* _GetConfig();

public:
	GeneratorSingle(ITCBuffer* buffer, GeneratorSingleConfig* config);
	
	~GeneratorSingle(void);
	
	QueueDataBase* GenerateData() override;

	GeneratorSingleConfig* GetConfig();
};

