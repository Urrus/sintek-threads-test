#pragma once
#include <mutex>
#include <iostream>

//���������������� ������� ��� ������ � �����, ���� std::ostream
//����������� � ������� ��������.
class ThreadSafeCout
{
protected:
	//����� ��� ������
	std::ostream* _out;

	std::mutex _mtx;

public:
	ThreadSafeCout(std::ostream* out);

	~ThreadSafeCout(void);

	template<class T> ThreadSafeCout& operator<<(const T& msg)
	{
		_mtx.lock();
		*_out << msg;
		_mtx.unlock();

		return *this;
	}
};

