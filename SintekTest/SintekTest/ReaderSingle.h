#pragma once
#include "ReaderBase.h"
#include "ITCBuffer.h"
#include "General.h"
#include "GenDataSingleVal.h"
#include <vector>
#include <map>

//���������� ��������� � ������������ ��������� �����
//��� ��������� ���������� �������� �� ������, �� �������� ��� � ���� ������������� ���������
//��������������� �������������� ��� ������ ���������� �������� ��� ������� �������� �����. 
//��� ������ ���� ������������������, �������� ��������� �������� ������������ � ������������ � �������� ���������� �������.
class ReaderSingle : 
	public ReaderBase
{
protected:
	//����� �������� ��� �������� ��������� � ���������� ��������� ������
	std::map<color, std::vector<GenDataSingleVal*>> _elmList;

protected:
	void _ProcessData(QueueDataBase* data) override;

	//��������� ����� ������� � ��������������� ������
	void _PlaceNewElement(GenDataSingleVal* element);

	//������� ������ ��� ������������� �����
	void _ClearVector(color color);

	virtual void _AfterThreadStarted() override;

public:
	ReaderSingle(ITCBuffer* buffer, ReaderSingleConfig* config);
	
	~ReaderSingle(void);

	//�������� ������������� ������������������ ������������ ���������
	void GetOrderedData(std::vector<GenDataSingleVal*>& vect);

	//������� ����������� ������
	void ClearData();
};

