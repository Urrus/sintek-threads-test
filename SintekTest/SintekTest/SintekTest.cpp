// SintekTest.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "Generator.h"
#include "GeneratorBase.h"
#include "GeneratorSingle.h"
#include "ITCBuffer.h"
#include <iostream>
#include <Windows.h>
#include "ReaderBase.h"
#include "ReaderSingle.h"
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <sstream>

using namespace std;

//������ �������� ������������ ���������� (� ��� ����� � ��������� ������� ��� ������) �� ��������� �����
int ReadMainConfig(string path, MainConfig& config)
{
	using namespace std;
	ifstream in(path);
	if(in.is_open())
	{
		string orderStr;
		in >> orderStr;
		if(orderStr.size() < 3)
		{
			cout << "Error while reading color order.";
			return 1;
		}

		config.orderStr = orderStr;

		for(int i = 0; i < 3; ++i)
		{
			color c;
			switch(orderStr[i])
			{
				case 'R':
					c = SNT_RED;
					break;
				case 'G':
					c = SNT_GREEN;
					break;
				case 'B':
					c = SNT_BLUE;
					break;
				default:
					cout << "Error while reading color order.";
					return 1;
					break;
			}
			config.order[c] = i;   
		}
		if(config.order.size() != 3)
		{
			cout << "Error while reading color order.";
			return 1;
		}
	}
	else
	{
		cout << "Error while opening config file.";
		return 1;
	}
	return 0;
}

//������ ����� �� ��������
char colC(color c)
{
	char res = '_';
	switch (c)
	{
		case SNT_RED:
			res = 'r';
			break;
		case SNT_GREEN:
			res = 'g';
			break;
		case SNT_BLUE:
			res = 'b';
			break;
		default:
			break;
	}
	return res;
}

//������������� ���������� 
void InitGeneratorSingle(GeneratorSingle** gen, ITCBuffer* buffer)
{
	auto genCfg = new GeneratorSingleConfig();
	
	genCfg->genAmount = 0;
	genCfg->genPeriodMill = 1;

	*gen = new GeneratorSingle(buffer, genCfg);
}

//������������� �����������
void InitReaderSingle(ReaderSingle** read, ITCBuffer* buffer, MainConfig* mainCfg)
{
	auto readCfg = new ReaderSingleConfig();

	readCfg->order = mainCfg->order;
	readCfg->readPeriodMill = 1;

	*read = new ReaderSingle(buffer, readCfg);
}

//������� ��������, ���������� ��� ������������
void TestCleanup(vector<GenDataSingleVal*>& preSequence, vector<GenDataSingleVal*>& testSequence)
{
	for(int i = 0; i < testSequence.size(); ++i)
	{
		auto elm = *testSequence.rbegin();
		delete elm;
		testSequence.pop_back();
	}
}

//������ ������ �����������
void StartReader(ReaderBase* reader)
{
	if(!reader->IsRunning())
	{
		tsout << "Starting reader...\n";
		reader->StartThread();
		tsout << "Reader is working now!\n";
	}
}

//������ ������ ����������
void StartGenerator(GeneratorBase* generator)
{
	if(!generator->IsRunning())
	{
		tsout << "Starting generator...\n";
		generator->StartThread();
		tsout << "Generator is working now!\n";
	}
}

//��������� �����������
void StopReader(ReaderBase* reader)
{
	if(reader->IsRunning())
	{
		tsout << "Stopping reader...\n";
		reader->StopThread();
		tsout << "Reader has been stopped\n";
	}
}

//��������� ����������
void StopGenerator(GeneratorBase* generator)
{
	if(generator->IsRunning())
	{
		tsout << "Stopping generator...\n";
		generator->StopThread();
		tsout << "Generator has been stopped\n";
	}
}

//������ ������������ ��� ��������� (� ������� ������)
//minSize - ����������� ������ �������� ������������������
//maxSize - ������������ ������ �������� ������������������
bool RunTestSingle(MainConfig& mainConfig, int minSize, int maxSize)
{
	//������������ ������ ���������
	tsout << "Test started...\n";
	//��� ���������� ��������� ������������ ������������������ ����� �� 100 - 200 ���������
	vector<GenDataSingleVal*> preSequence;
	//������� ������������������ ��� �������� ����, ��� ���������� �� �������� ���������� ���������
	vector<GenDataSingleVal*> testSequence;
	
	//��������� ����� ��� �����
	ITCBuffer buffer;
	int diff = maxSize - minSize;
	int seqSize = minSize + (diff != 0 ? rand() % (diff + 1) : 0);
	tsout << "Generating test sequence of " << seqSize << " elements...\n";
	
	preSequence.resize(seqSize);
	testSequence.resize(seqSize);

	for(int i = 0; i < seqSize; ++i)
	{
		int col = rand() % 3;
		preSequence[i] = new GenDataSingleVal(col, i);
		testSequence[i] = new GenDataSingleVal(col, i);
	}

	//�������� ������ ���������� ��� �����
	auto genCfg = new GeneratorSingleConfig();
	
	genCfg->genAmount = preSequence.size();
	genCfg->genPeriodMill = 1;
	genCfg->sequence = &preSequence;

	GeneratorSingle generator(&buffer, genCfg);

	//�������� ������ ����������� ��� �����
	auto readCfg = new ReaderSingleConfig();
	
	readCfg->order = mainConfig.order;
	readCfg->readAmount = preSequence.size();
	readCfg->readPeriodMill = 1;

	ReaderSingle reader(&buffer, readCfg);

	StartGenerator(&generator);
	StartReader(&reader);

	if(!generator.IsRunning())
	{
		TestCleanup(preSequence, testSequence);
		tsout << "Generator is not working.\n";	
		return false;
	}
	else if(!reader.IsRunning())
	{
		TestCleanup(preSequence, testSequence);
		tsout << "Reader is not working.\n";
		return false;
	}

	while(reader.IsRunning()) { }
	
	tsout << "Reading elements finished\n";

	//�������, ����� ���������� ����� ����������
	if(generator.IsRunning())
	{
		Sleep(1000);
		if(generator.IsRunning())
		{
			TestCleanup(preSequence, testSequence);
			tsout << "Generator thread has not been stopped.\n";
			generator.StopThreadAsync();
			return false;
		}
	}

	vector<GenDataSingleVal*> redSequence;
	reader.GetOrderedData(redSequence);

	tsout << "\nColor order: " << mainConfig.orderStr << "\n";

	tsout << "\nResult ordered sequence: [id]:value\n";
	for(int i = 0; i < redSequence.size(); ++i)
	{
		auto elm = redSequence[i];
		tsout << "[" << elm->GetId() << "]:" << colC(elm->GetColor()) << (i == redSequence.size() - 1 ? ";" : ", ");
	}
	tsout << "\n";
	tsout << "\nTest unordered sequence:\n";
	for(int i = 0; i < testSequence.size(); ++i)
	{
		auto elm = testSequence[i];
		tsout << "[" << elm->GetId() << "]:" << colC(elm->GetColor()) << (i == testSequence.size() - 1 ? ";" : ", ");
	}
	tsout << "\n";

	if(redSequence.size() != testSequence.size())
	{
		TestCleanup(preSequence, testSequence);
		tsout << "redSequence.size() != testSequence.size()\n";
		return false;
	}
	
	for(int i = 0; i < redSequence.size(); ++i)
	{
		auto elm = redSequence[i];
		if(elm == NULL)
		{
			TestCleanup(preSequence, testSequence);
			tsout << "redSequence[i] == NULL\n";
			return false;
		}
		else
		{
			int j = 0;
			for(; j < testSequence.size(); ++j)
			{
				if(testSequence[i]->GetId() == elm->GetId())
				{
					if(testSequence[i]->GetColor() != elm->GetColor())
					{
						TestCleanup(preSequence, testSequence);
						tsout << "Colors do not match for the id=" << elm->GetId() << "\n";
						return false;
					}
				}
			}
		}
		if(i != 0)
		{
			int cmp = ColorCompare(elm->GetColor(), redSequence[i - 1]->GetColor(), readCfg->order);
			if(cmp < 0)
			{
				TestCleanup(preSequence, testSequence);
				tsout << "Order in result sequence is broken at index: " << i << "\n";
				return false;
			}
		}
	}

	TestCleanup(preSequence, testSequence);
	return true;
}

//����� ����
void RenderMenu()
{
		tsout << "\nCommands:\n \
sg - start generator\n \
eg - stop generator\n \
sr - start reader\n \
er - stop reader\n \
h - show this command list\n \
t - run test\n \
or - show current reader ordered data\n \
cr - clear reader buffer\n \
q - quit\n ";
}

string usage = "Enter path to file with app config.\n";

int main(int argc, char** argv)
{
	//TODO: debug string for detour input parsing
	string cfgPath = "C://CodeStuff//SintekTest//app.cfg";
	/*////

	//input args parsing
	if(argc < 2 && false)
	{
		cout << usage;
		return 1;
	}

	
	string cfgPath(argv[1]);
	////*/
	MainConfig mainConfig;
	if(ReadMainConfig(cfgPath, mainConfig) != 0)
	{
		cout << "Main config reading failed. Exiting now." << endl;
		return 1;
	}

	ITCBuffer buffer;

	GeneratorSingle* generator;
	InitGeneratorSingle(&generator, &buffer);

	ReaderSingle* reader;
	InitReaderSingle(&reader, &buffer, &mainConfig);

	RenderMenu();

	bool isExit = false;
	string com;
	while(!isExit)
	{
		cin >> com;

		if("sg" == com)
		{
			StartGenerator(generator);
		}
		else if("eg" == com)
		{
			StopGenerator(generator);
		}
		else if("sr" == com)
		{
			StartReader(reader);
		}
		else if("er" == com)
		{
			StopReader(reader);
		}
		else if("h" == com)
		{
			RenderMenu();
		}
		else if("t" == com)
		{
			if(!generator->IsRunning() && !reader->IsRunning())
			{
				auto res = RunTestSingle(mainConfig, 50, 70);
				if(!res)
				{
					tsout << "\nTest failed.\n";
				}
				else
				{
					tsout << "\nTest passed!\n";
				}
				RenderMenu();
			}
		}
		else if("q" == com)
		{
			tsout << "Exiting...\n";
			StopGenerator(generator);
			StopReader(reader);

			isExit = true;
		}
		else if("or" == com)
		{
			vector<GenDataSingleVal*> data;
			reader->GetOrderedData(data);
			ostringstream os;
			os << "\Current reader data ([id]:value):\n";
			os << "Color order: " << mainConfig.orderStr << "\n";
			os << "Count: " << data.size() << "\n";
			for(int i = 0; i < data.size(); ++i)
			{
				os << "[" << data[i]->GetId() <<"]:" << colC(data[i]->GetColor()) << (i == data.size() - 1 ? ";\n" : ", ");
			}
			tsout << os.str() << "\n";
		}
		else if("cr" == com)
		{
			if(!reader->IsRunning())
			{
				reader->ClearData();
			}
		}
	}

	return 0;
}	