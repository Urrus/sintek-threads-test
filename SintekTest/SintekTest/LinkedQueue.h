#pragma once
#include "QueueElement.h"

//�������, ������������� � ���� ���������������� ������.
//����������� ������ �� ������ ������� � ������, ����� �������, ���� �������� ������� QueueElement->next ��������� �� �������� �������, ������� ������������� ����� � ����� �������,
//���� QueueElement->prev - �� ������, ������� ����� � ������ �������.
class LinkedQueue
{
protected:
	//���-�������� ������� �������, ��� _next ��������� �� ������ ������� � �������, _prev == NULL
	QueueElement _head;
	
	//����-��������� ������� �������, ��� _next == NULL, _prev ��������� �� ��������� ������� �������
	QueueElement _tail;
	
public:
	LinkedQueue(void);

	virtual ~LinkedQueue(void);
	
	//���������� ������ �������� � ������ �������
	void PushElement(QueueDataBase* data);

	//���������� �������� �� ������ �������
	bool PopElement(QueueDataBase** data);

	//TRUE, ���� ������� ����� (_head->next == _tail)
	bool IsEmpty();

	QueueElement* GetHead();

	QueueElement* GetTail();
};

