#include "stdafx.h"
#include "ReaderSingle.h"
#include <iostream>
#include "GenDataSingleVal.h"

using namespace std;

ReaderSingle::ReaderSingle(ITCBuffer* buffer, ReaderSingleConfig* config)
	: ReaderBase(buffer, config)
{
}

ReaderSingle::~ReaderSingle(void)
{
	ClearData();
	_elmList.clear();
}

void ReaderSingle::_ProcessData(QueueDataBase* data)
{
	auto elm = (GenDataSingleVal*) data;
#ifndef SNT_DEBUG_MUTE
	tsout << "Data got: " << elm->GetId() << "; color=" << elm->GetColor() << "\n";
#endif
	_PlaceNewElement(elm);
}

void ReaderSingle::_PlaceNewElement(GenDataSingleVal* element)
{
	color c = element->GetColor();
	_elmList[c].push_back(element);
}

void ReaderSingle::GetOrderedData(std::vector<GenDataSingleVal*>& vect)
{
	auto rVect = _elmList[SNT_RED];
	auto gVect = _elmList[SNT_GREEN];
	auto bVect = _elmList[SNT_BLUE];

	vector<color> colorOrder(3);
	colorOrder[_config->order[SNT_RED]] = SNT_RED;
	colorOrder[_config->order[SNT_GREEN]] = SNT_GREEN;
	colorOrder[_config->order[SNT_BLUE]] = SNT_BLUE;

	for(int i = 0; i < 3; ++i)
	{
		vector<GenDataSingleVal*>& v = _elmList[colorOrder[i]];
		for(int j = 0; j < v.size(); ++j)
		{
			vect.push_back(v[j]);
		}
	}
}

void ReaderSingle::ClearData()
{
	_redCount = 0;
	_ClearVector(SNT_RED);
	_ClearVector(SNT_GREEN);
	_ClearVector(SNT_BLUE);
}

void ReaderSingle::_ClearVector(color color)
{
	int size = _elmList[color].size();
	for(int i = 0; i < size; ++i)
	{
		auto elm = *_elmList[color].rbegin();
		_elmList[color].pop_back();
		delete elm;
	}
}

void ReaderSingle::_AfterThreadStarted()
{
	ReaderBase::_AfterThreadStarted();
}