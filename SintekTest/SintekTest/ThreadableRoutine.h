#pragma once
#include <thread>

//������� ����� ��� ������� ��������� � ��������� ������
class ThreadableRoutine
{
protected:
	//������ ������
	std::thread _mainThread;

	//�����, ������������� � ��������� ������
	//� ��� � ����� ���������� _MTRStep(), ���� ���������� �� ����� ��������.
	virtual void _MainThreadRoutine();

	//������� ��������� ���������� � ��������� ������ ������-���� �������������� ���������
	virtual void _MTRStep() = 0;

	//����, ������������, ����������� �� �����
	bool _isRunning;

	//����, ���������������, ��� ���������� ��������� ���������� ������
	bool _isStoppingThread;
protected:
	//����������� ����� ������� ������
	virtual void _AfterThreadStarted();

	//����������� ����� ���������� ������
	virtual void _BeforeThreadStopped();

public:
	ThreadableRoutine(void);

	~ThreadableRoutine(void);
	
	//����������� �� �����?
	virtual bool IsRunning();

	//������ ���������� ������
	virtual void StartThread();

	//���������� ���������� ������ (� ��������� ���������� ��������)
	virtual void StopThread();

	//������������� ���������� ������ ������ ��� �������� ����������
	virtual void StopThreadAsync();

	//�������� ���������� ������ ������
	virtual void WaitForThreadStop();
};

