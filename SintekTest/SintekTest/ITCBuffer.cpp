#include "stdafx.h"
#include "ITCBuffer.h"
#include <iostream>
#include <Windows.h>
#include "General.h"

using namespace std;

ITCBuffer::ITCBuffer(void)
	: LinkedQueue()
{
}

ITCBuffer::~ITCBuffer(void)
{
}

int ITCBuffer::AddElement(QueueDataBase* data)
{
	//��������� ������� ��� ��������
#ifndef SNT_DEBUG_MUTE
	bool ml = _mtx.try_lock();
	if(!ml)
	{
		tsout << "MUTEX IS LOCKED WHILE ADDING ELEMENT";
		_mtx.lock();
	}
#else
	_mtx.lock();
#endif
	auto lt2 = _IsCountLessTwo();
	if(!lt2)
	{
		_mtx.unlock();
	}
#ifdef SNT_DEBUG_THREADSLEEP
	//**remove after debug**//
	Sleep(100);
	//**********************//
#endif
	PushElement(data);

	if(lt2)
	{
		_mtx.unlock();
	}

	return 0;
}

bool ITCBuffer::GetData(QueueDataBase** data)
{
#ifndef SNT_DEBUG_MUTE
	bool ml = _mtx.try_lock();
	if(!ml)
	{
		tsout << "MUTEX IS LOCKED WHILE GETTING DATA";
		_mtx.lock();
	}
#else
	_mtx.lock();
#endif
	auto lt2 = _IsCountLessTwo();
	if(!lt2)
	{
		_mtx.unlock();
	}
#ifdef SNT_DEBUG_THREADSLEEP
	//**remove after debug**//
	Sleep(100);
	//**********************//
#endif
	bool stat = PopElement(data);

	if(lt2)
	{
		_mtx.unlock();
	}
	return stat;
}

bool ITCBuffer::_IsCountLessTwo()
{
	if(_head.GetNext() == &_tail || _head.GetNext()->GetNext() == &_tail)
	{
		return false;
	}
	return true;
}