#pragma once
#include "QueueElement.h"
#include "GenDataSingleVal.h"
#include "ITCBuffer.h"
#include "General.h"
#include <thread>
#include "ThreadableRoutine.h"

//������� ����� ��� ����������
class GeneratorBase : public ThreadableRoutine
{
protected:
	//������ �� ����� ��� ������
	ITCBuffer* _buffer;

	//��������� ����������
	GeneratorBaseConfig* _config;
	
	//����� ���������� ������ ������ �������� ������ � ��������� ������ � ms, ����������� ��� ������ ��� � ��������� � ������������ ��������
	double _lastT;

	//������� ��������������� ���������
	long int _genCount;

	//������ ������ �� ��������������� ��������
	std::vector<QueueDataBase*> _generated;

protected:
	void _MTRStep() override;

	virtual void _AfterThreadStarted() override;

public:
	GeneratorBase(ITCBuffer* buffer, GeneratorBaseConfig* config);
	
	virtual ~GeneratorBase(void);
	
	//����������� ����� ��� ��������� ������ �������� ��� �����
	//������� ����� ������ ��������� ���� ����� ��������� ������ ��������, 
	//����������� ��������� ������ (��������, ���� �������� �����, ��� ����� ������������������), ������� ����� ����� ������ �������� �����������.
	virtual QueueDataBase* GenerateData() = 0;

	long int GetGenCount();

	std::vector<QueueDataBase*> GetGeneratedElements();

	GeneratorBaseConfig* GetBaseConfig();

	void SetBaseConfig(GeneratorBaseConfig* config);
};

