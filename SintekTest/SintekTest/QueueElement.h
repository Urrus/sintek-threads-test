#pragma once
#include "QueueDataBase.h"

//������� �������, ����������� � �������� ������
class QueueElement
{
private:
	//��������� �� ��������� ������� � �������
	QueueElement* _next;
	
	//��������� �� ���������� ������� �������
	QueueElement* _prev;

	//��������� �� ������ �������� �������
	QueueDataBase* _data;

public:
	QueueElement(void);
	
	QueueElement(QueueDataBase* data);
	
	virtual ~QueueElement(void);

	QueueElement* GetNext();
	void SetNext(QueueElement* elm);

	QueueElement* GetPrev();
	void SetPrev(QueueElement* elm);

	QueueDataBase* GetData();
	void SetData(QueueDataBase* data);
};

