#include "stdafx.h"
#include "GenDataSingleVal.h"


GenDataSingleVal::GenDataSingleVal(color data)
{
	_color = data;
}

GenDataSingleVal::GenDataSingleVal(color data, long int id)
{
	_color = data;
	_id = id;
}

GenDataSingleVal::~GenDataSingleVal(void)
{
}

color GenDataSingleVal::GetColor()
{
	return _color;
}

long int GenDataSingleVal::GetId()
{
	return _id;
}
