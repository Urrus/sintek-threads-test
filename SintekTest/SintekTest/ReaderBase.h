#pragma once
#include "QueueElement.h"
#include "ITCBuffer.h"
#include "General.h"
#include <thread>
#include "ThreadableRoutine.h"

//������� ����� ��� �����������, ��������� ������ �� ������
class ReaderBase : public ThreadableRoutine
{
protected:
	//��������� �� ����� ��� ������
	ITCBuffer* _buffer;

	//��������� �����������
	ReaderBaseConfig* _config;

	//����� ���������� ������ ������ ������ ������ � ��������� ������ � ms, ����������� ��� ������ ��� � ��������� � ������������ ��������
	double _lastT;

	//������� ����������� ���������
	long int _redCount;

protected:
	void _MTRStep() override;
	
	//����������� ����� ��� ��������� �������� �������
	virtual void _ProcessData(QueueDataBase* data) = 0;

	virtual void _AfterThreadStarted() override;

public:
	ReaderBase(ITCBuffer* buffer, ReaderBaseConfig* config);
	
	virtual ~ReaderBase(void);
	
	//����� ��� ��������� ������ �������� �� ������� � ������ ��� ���������
	void ProcessNextData();

	ReaderBaseConfig* GetBaseConfig();

	void SetBaseConfig(ReaderBaseConfig* config);
};

