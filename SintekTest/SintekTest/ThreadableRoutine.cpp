#include "stdafx.h"
#include "ThreadableRoutine.h"
#include <iostream>
#include "General.h"

using namespace std;

ThreadableRoutine::ThreadableRoutine(void)
{
	_isRunning = false;
	_isStoppingThread = false;
}

ThreadableRoutine::~ThreadableRoutine(void)
{
	if(_mainThread.joinable())
	{
		_mainThread.join();
	}
}

void ThreadableRoutine::StartThread()
{
	if(!IsRunning())
	{
		_isStoppingThread = false;
		_isRunning = false;
		_mainThread = std::thread(&ThreadableRoutine::_MainThreadRoutine, this);
	}
}

bool ThreadableRoutine::IsRunning()
{
	return _isRunning;
}

void ThreadableRoutine::_MainThreadRoutine()
{
	_isRunning = true;
	_AfterThreadStarted();
	while(!_isStoppingThread)
	{
		this->_MTRStep();
	}
	_BeforeThreadStopped();
	_isRunning = false;
	_isStoppingThread = false;	
}

void ThreadableRoutine::StopThreadAsync()
{
	if(_isRunning && !_isStoppingThread)
	{
		_isStoppingThread = true;
	}
}

void ThreadableRoutine::StopThread()
{
	if(_isRunning && !_isStoppingThread)
	{
		_isStoppingThread = true;
		WaitForThreadStop();
		_isStoppingThread = false;
		_isRunning = false;
	}
}

void ThreadableRoutine::WaitForThreadStop()
{
	if(_isRunning)
	{
		_mainThread.join();
#ifndef SNT_DEBUG_MUTE
		tsout << "\nmain thread has been joined\n";
#endif
	}
}


void ThreadableRoutine::_AfterThreadStarted()
{
}

void ThreadableRoutine::_BeforeThreadStopped()
{
}