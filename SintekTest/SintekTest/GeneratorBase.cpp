#include "stdafx.h"
#include "GeneratorBase.h"
#include <future>
#include <iostream>
#include <time.h>
#include "General.h"

using namespace std;

GeneratorBase::GeneratorBase(ITCBuffer* buffer, GeneratorBaseConfig* config)
{
	_config = config;
	_buffer = buffer;
	_lastT = 0;
}

GeneratorBase::~GeneratorBase(void)
{
	delete _config;
}

void GeneratorBase::_MTRStep()
{
	auto period = _config->genPeriodMill;
	auto curT = clock() / (double) CLOCKS_PER_SEC * 1000;
	
	//��������� ������ �������� � �������� � ������������ ��������
	if((curT - _lastT) > period)
	{
		auto data = GenerateData();
		_buffer->AddElement(data);
		_lastT = curT;

		_generated.push_back(data);
		++_genCount;
		if(_config->genAmount > 0 && _genCount >= _config->genAmount)
		{
			//������������� �����, ���� ������������� �������� ���������� ���������
			_isStoppingThread = true;
		}
	}
}

void GeneratorBase::_AfterThreadStarted()
{
	_genCount = 0;
	_generated.clear();
}

long int GeneratorBase::GetGenCount()
{
	return _genCount;
}

std::vector<QueueDataBase*> GeneratorBase::GetGeneratedElements()
{
	return _generated;
}

GeneratorBaseConfig* GeneratorBase::GetBaseConfig()
{
	return _config;
}

void GeneratorBase::SetBaseConfig(GeneratorBaseConfig* config)
{
	_config = config;
}