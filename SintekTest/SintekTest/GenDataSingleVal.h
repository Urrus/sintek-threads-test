#pragma once
#include "color.h"
#include "QueueDataBase.h"
#include <vector>

//������� ������� � ������� � ���� ������������� �������� �����
class GenDataSingleVal :
	public QueueDataBase
{
protected:
	color _color;

	//id �������, ����������� ����������� ������
	//������������ � ����� ��� �������� ����, ��� ���������� �� ������ ����� ����� � �������
	long int _id;

public:
	GenDataSingleVal(color data);

	GenDataSingleVal(color data, long int id);

	~GenDataSingleVal(void);

	color GetColor();
	
	long int GetId();
};

