#include "stdafx.h"
#include "ReaderBase.h"
#include <iostream>

using namespace std;

ReaderBase::ReaderBase(ITCBuffer* buffer, ReaderBaseConfig* config)
{
	_buffer = buffer;
	_config = config;
	_lastT = 0;
	_redCount = 0;
}

ReaderBase::~ReaderBase(void)
{
	delete _config;
}

void ReaderBase::_MTRStep()
{
	auto period = _config->readPeriodMill;
	auto curT = clock() / (double) CLOCKS_PER_SEC * 1000;
	
	//������ ���������� �������� � �������� � ������������ ��������
	if((curT - _lastT) > period)
	{
		ProcessNextData();
		_lastT = curT;

		if(_config->readAmount > 0 && _redCount >= _config->readAmount)
		{
			//������������� �����, ���� ��������� �������� ���������� ���������
			_isStoppingThread = true;
		}
	}
}

void ReaderBase::ProcessNextData()
{
	QueueDataBase* ptr = NULL;
	auto succ = _buffer->GetData(&ptr);
	if(succ)
	{
		++_redCount;
		this->_ProcessData(ptr);
	}
	else
	{
#ifndef SNT_DEBUG_MUTE
		tsout << "No data was obtained." << "\n";
#endif
	}
}

ReaderBaseConfig* ReaderBase::GetBaseConfig()
{
	return _config;
}

void ReaderBase::SetBaseConfig(ReaderBaseConfig* config)
{
	_config = config;
}

void ReaderBase::_AfterThreadStarted()
{
	//_redCount = 0;
}
