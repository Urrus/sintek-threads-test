#include "stdafx.h"
#include "LinkedQueue.h"


LinkedQueue::LinkedQueue(void)
{
	_head.SetNext(&_tail);
	_head.SetPrev(NULL);

	_tail.SetNext(NULL);
	_tail.SetPrev(&_head);
}


LinkedQueue::~LinkedQueue(void)
{
	QueueDataBase* elm = NULL;
	while(!IsEmpty())
	{
		PopElement(&elm);
		delete elm;
	}
}

void LinkedQueue::PushElement(QueueDataBase* data)
{
	auto elm = new QueueElement();
	elm->SetData(data);

	//������� ����� � ������ ��������
	elm->SetNext(&_tail);
	elm->SetPrev(_tail.GetPrev());

	//����������� ����� � ����-���������� � ���������� ��������� �������
	_tail.GetPrev()->SetNext(elm);
	_tail.SetPrev(elm);
}

bool LinkedQueue::IsEmpty()
{
	if(_head.GetNext() == &_tail)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool LinkedQueue::PopElement(QueueDataBase** data)
{
	if(IsEmpty())
	{
		return false;
	}

	auto hNext = _head.GetNext();
	*data = hNext->GetData();

	_head.SetNext(hNext->GetNext());
	hNext->GetNext()->SetPrev(&_head);

	delete hNext;

	return true;
	
}

QueueElement* LinkedQueue::GetHead()
{
	return &_head;
}

QueueElement* LinkedQueue::GetTail()
{
	return &_tail;
}
