#include "stdafx.h"
#include "color.h"

int ColorCompare(const color& c1, const color& c2, const std::map<color, int>& order)
{
	if(c1 == c2)
	{
		return 0;
	}
	else if(order.at(c1) > order.at(c2))
	{
		return 1;
	}
	else
	{
		return -1;
	}
}