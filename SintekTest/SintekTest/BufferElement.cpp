#include "stdafx.h"
#include "QueueElement.h"


QueueElement::QueueElement(void)
{

}

QueueElement::QueueElement(QueueDataBase* data)
{
	_data = data;
}

QueueElement::~QueueElement(void)
{

}

QueueElement* QueueElement::GetNext()
{
	return _next;
}

void QueueElement::SetNext(QueueElement* elm)
{
	_next = elm;
}


QueueElement* QueueElement::GetPrev()
{
	return _prev;
}

void QueueElement::SetPrev(QueueElement* elm)
{
	_prev = elm;
}

QueueDataBase* QueueElement::GetData()
{
	return _data;
}

void QueueElement::SetData(QueueDataBase* data)
{
	_data = data;
}
